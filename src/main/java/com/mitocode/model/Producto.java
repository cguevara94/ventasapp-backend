package com.mitocode.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Entity
@Table(name = "producto")
@Data
@JsonInclude(value = Include.NON_NULL)
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "productoSeq")
	@SequenceGenerator(name = "productoSeq", sequenceName = "seq_producto")
	private Long idProducto;
	
	@Size(min = 3, message = "{producto.nombre.size}")
	@NotNull(message = "{producto.nombre.null}")
	@Column(name = "nombre")
	private String nombre;
	
	@NotNull(message = "{producto.marca.null}")
	@Size(min = 3, message = "{producto.marca.size}")
	@Column(name = "marca")
	private String marca;
	
	@NotNull(message = "{producto.precio.null}")
	@DecimalMin(value = "0.01", message = "{producto.precio.min}")
	@Column(name = "precio")
	private BigDecimal precio;
	
}
