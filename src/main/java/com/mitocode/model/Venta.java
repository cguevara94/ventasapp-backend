package com.mitocode.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "venta")
@Getter
@Setter
@JsonInclude(value = Include.NON_NULL)
public class Venta {

	@Id
	@GeneratedValue(generator = "ventaSeq", strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "ventaSeq", sequenceName = "seq_venta")
	private Long idVenta;

	@NotNull(message = "{venta.fecha.null}")
	@Column(name = "fecha")
	private LocalDateTime fecha;

	@NotNull(message  = "{venta.persona.null}")
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_persona", nullable = false, foreignKey = @ForeignKey(name = "fk_venta_persona"))
	private Persona persona;

	@Setter(AccessLevel.NONE)
	@Column(name = "importe")
	private BigDecimal importe;

	@OneToMany(mappedBy = "venta", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<DetalleVenta> detalleVentas;

	@PrePersist
	public void prePersist() {
		calcularImporte();
	}
	
	@PreUpdate
	public void preUpdate() {
		calcularImporte();
	}
	
	private void calcularImporte() {
		importe = BigDecimal.ZERO;
		
		detalleVentas.forEach(detalle -> {
			importe = importe.add(detalle.calcularTotalDetalle());
		});
	}
	
}
