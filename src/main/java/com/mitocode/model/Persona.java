package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Entity
@Table(name = "persona")
@Data
@JsonInclude(value = Include.NON_NULL)
public class Persona {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "personaSeq")
	@SequenceGenerator(name = "personaSeq", sequenceName = "seq_persona")
	private Long idPersona;
	
	@Pattern(regexp = "^[a-zA-Z\\s]*$", message = "{persona.nombres.caracteres}")
	@Size(min = 3, message = "{persona.nombres.size}")
	@NotNull(message = "{persona.nombres.null}")
	@Column(name = "nombres")
	private String nombres;
	
	@Pattern(regexp = "^[a-zA-Z\\s]*$", message = "{persona.apellidos.caracteres}")
	@Size(min = 3, message = "{persona.apellidos.size}")
	@NotNull(message = "{persona.apellidos.null}")
	@Column(name = "apellidos")
	private String apellidos;
	

}
