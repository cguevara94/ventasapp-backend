package com.mitocode.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Entity
@Table(name = "detalle_venta")
@Data
@JsonInclude(value = Include.NON_NULL)
public class DetalleVenta {
	
	@Id
	@GeneratedValue(generator = "detalleVentaSeq", strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "detalleVentaSeq", sequenceName = "seq_detalle_venta")
	private Long idDetalleVenta;
	
	@JsonIgnore
	@ManyToOne(optional = false)
	@JoinColumn(nullable = false, name = "id_venta", foreignKey = @ForeignKey(name="fk_detalle_v_venta"))
	private Venta venta;
	
	@NotNull(message = "{detalleVenta.producto.null}")
	@ManyToOne(optional = false)
	@JoinColumn(nullable = false, name = "id_producto", foreignKey = @ForeignKey(name="fk_detalle_v_producto"))
	private Producto producto;
	
	@NotNull(message = "{detalleVenta.cantidad.null}")
	@Min(value = 1, message = "{detalleVenta.cantidad.min}")
	@Column(name = "cantidad")
	private Integer cantidad;
	
	public BigDecimal calcularTotalDetalle() {
		return producto.getPrecio().multiply(BigDecimal.valueOf(cantidad));
	}

}
