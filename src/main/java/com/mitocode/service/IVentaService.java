package com.mitocode.service;

import com.mitocode.model.Venta;

public interface IVentaService extends ICRUD<Venta, Long> {

	public Venta registrarVentaConDetalle(Venta venta);
	
}
