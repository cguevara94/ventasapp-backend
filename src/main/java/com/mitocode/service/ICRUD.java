package com.mitocode.service;

import java.util.List;

public interface ICRUD<T, ID> {

	T crear(T entidad);	
	T modificar(T entidad);	
	T listarPorId(ID id);
	List<T> listar();
	void eliminar(T entidad);
	
}
