package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Venta;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IVentaRepo;
import com.mitocode.service.IProductoService;
import com.mitocode.service.IVentaService;

@Service
public class VentaServiceImpl extends CRUDImpl<Venta, Long> implements IVentaService {

	@Autowired
	private IVentaRepo ventaRepo;
	@Autowired
	private IProductoService productoService;

	@Override
	protected IGenericRepo<Venta, Long> obtenerRepo() {
		return ventaRepo;
	}

	@Override
	public Venta registrarVentaConDetalle(Venta venta) {
		venta.getDetalleVentas().forEach(detalle -> { 
			detalle.setVenta(venta);
			detalle.setProducto(productoService.listarPorId(detalle.getProducto().getIdProducto()));
		});
		
		return ventaRepo.save(venta);
	}
	
}
