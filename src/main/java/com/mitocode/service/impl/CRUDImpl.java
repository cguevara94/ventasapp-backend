package com.mitocode.service.impl;

import java.util.List;

import com.mitocode.repo.IGenericRepo;
import com.mitocode.service.ICRUD;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {

	protected abstract IGenericRepo<T, ID> obtenerRepo();
	
	@Override
	public T crear(T entidad) {
		return obtenerRepo().save(entidad);
	}

	@Override
	public T modificar(T entidad) {
		return obtenerRepo().save(entidad);
	}

	@Override
	public T listarPorId(ID id) {
		return obtenerRepo().findById(id).orElse(null);
	}

	@Override
	public List<T> listar() {
		return obtenerRepo().findAll();
	}

	@Override
	public void eliminar(T entidad) {
		obtenerRepo().delete(entidad);
	}

}
