package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Producto;
import com.mitocode.repo.IGenericRepo;
import com.mitocode.repo.IProductoRepo;
import com.mitocode.service.IProductoService;

@Service
public class ProductoServiceImpl extends CRUDImpl<Producto, Long> implements IProductoService {

	@Autowired
	private IProductoRepo productoRepo;
	
	@Override
	protected IGenericRepo<Producto, Long> obtenerRepo() {
		return productoRepo;
	}

}
