package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.EntidadNoExisteException;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService ventaService;

	@GetMapping
	public ResponseEntity<List<Venta>> listar() {
		List<Venta> ventas = ventaService.listar();
		
		return new ResponseEntity<List<Venta>>(ventas, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Venta> buscarPorId(@PathVariable("id") Long id) {
		Venta venta = ventaService.listarPorId(id);

		if (Objects.isNull(venta)) {
			throw new EntidadNoExisteException("No existe venta para el id: " + id);
		}

		return new ResponseEntity<Venta>(venta, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Venta> crear(@Valid @RequestBody Venta venta) {
		Venta ventaCre = ventaService.registrarVentaConDetalle(venta);

		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(ventaCre.getIdVenta())
				.toUri();

		return ResponseEntity.created(uri).body(ventaCre);
	}

	@PutMapping
	public ResponseEntity<Venta> modificar(@Valid @RequestBody Venta venta) {
		Venta ventaAct = ventaService.modificar(venta);

		return new ResponseEntity<Venta>(ventaAct, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Long id) {
		Venta venta = ventaService.listarPorId(id);

		if (Objects.isNull(venta)) {
			throw new EntidadNoExisteException("No existe la venta a eliminar para el id: " + id);
		}
		
		ventaService.eliminar(venta);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
