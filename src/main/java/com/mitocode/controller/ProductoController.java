package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.EntidadNoExisteException;
import com.mitocode.model.Producto;
import com.mitocode.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

	@Autowired
	private IProductoService productoService;
	
	@GetMapping
	public ResponseEntity<List<Producto>> listar() {
		List<Producto> productos = productoService.listar();
		
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Producto> obtenerProducto(@PathVariable("id") Long id) {
		Producto producto = productoService.listarPorId(id);
		
		if (Objects.isNull(producto)) {
			throw new EntidadNoExisteException("No se encontro producto para el id: " + id);
		}
		
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Producto> crear(@Valid @RequestBody Producto producto) {
		Producto proCreado = productoService.crear(producto);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(proCreado.getIdProducto()).toUri();
		
		return ResponseEntity.created(uri).body(proCreado);
	}
	
	@PutMapping
	public ResponseEntity<Producto> actualizar(@Valid @RequestBody Producto producto) {
		Producto proAct = productoService.modificar(producto);
		return new ResponseEntity<Producto>(proAct, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Long id) {
		Producto productoEliminar = productoService.listarPorId(id);
		
		if (Objects.isNull(productoEliminar)) {
			throw new EntidadNoExisteException("No existe la entidad a eliminar para el id: " + id);
		}
		
		productoService.eliminar(productoEliminar);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
}
