package com.mitocode.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mitocode.exception.EntidadNoExisteException;
import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {
	
	@Autowired
	private IPersonaService personaService;
	
	@GetMapping
	public ResponseEntity<List<Persona>> listar() {
		List<Persona> personas = personaService.listar();
		
		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Persona> consultarPersona(@PathVariable("id") Long id) {
		Persona persona = personaService.listarPorId(id);
		
		if (Objects.isNull(persona)) {
			throw new EntidadNoExisteException("No se obtuvo resultado para el id: " + id);
		}
		
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Persona> crearPersona(@Valid @RequestBody Persona persona) {
		Persona perCreada = personaService.crear(persona);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(perCreada.getIdPersona()).toUri();
		
		return ResponseEntity.created(uri).body(perCreada);
	}
	
	@PutMapping
	public ResponseEntity<Persona> actualizarPersona(@Valid @RequestBody Persona persona) {
		Persona perAct = personaService.modificar(persona);
		return new ResponseEntity<Persona>(perAct, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Long id) {
		Persona persona = personaService.listarPorId(id);
		
		if (Objects.isNull(persona)) {
			throw new EntidadNoExisteException("No existe la persona a eliminar");
		}
		
		personaService.eliminar(persona);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

}
