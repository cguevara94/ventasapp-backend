package com.mitocode.exception;

public class EntidadNoExisteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5444612940422626334L;
	
	public EntidadNoExisteException(String mensaje) {
		super(mensaje);
	}

}
